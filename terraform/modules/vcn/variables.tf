variable "compartment_id" {
  description = "OCI compartment in which the VCN will exist"
}

variable "vcn_cidr_blocks" {
  description = "Cidr blocks for the VCN"
}

variable "vcn_display_name" {
  description = "Name for VCN"
}

variable "vcn_tags" {
  description = "Tags to assign to VCN"
}

variable "public_subnet_cidr_block" {
  description = "CIDR block of the public subnet"
}

variable "private_subnet_cidr_block" {
  description = "CIDR block of the private subnet"
}


variable "public_subnet_tags" {
  description = "Tags to assign to public subnet"
}

variable "private_subnet_tags" {
  description = "Tags to assign to private subnet"
}
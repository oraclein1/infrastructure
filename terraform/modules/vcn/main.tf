resource "oci_core_vcn" "main_vcn" {
  compartment_id = var.compartment_id

  cidr_blocks   = var.vcn_cidr_blocks
  display_name  = var.vcn_display_name
  freeform_tags = var.vcn_tags
}

resource "oci_core_subnet" "public_subnet" {
  cidr_block     = var.public_subnet_cidr_block
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.main_vcn.id

  freeform_tags = merge({
    "Subnet" : "public"
    },
    var.public_subnet_tags
  )
}

resource "oci_core_subnet" "private_subnet" {
  cidr_block     = var.private_subnet_cidr_block
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.main_vcn.id

  freeform_tags = merge({
    "Subnet" : "private"
    },
    var.private_subnet_tags
  )

  prohibit_public_ip_on_vnic = true
  route_table_id             = oci_core_route_table.private_subnet_route_table.id
}

resource "oci_core_internet_gateway" "internet_gateway" {
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.main_vcn.id
  display_name   = "Internet Gateway ${var.vcn_display_name}"
  enabled        = true
}

resource "oci_core_default_route_table" "default_route_table" {
  manage_default_resource_id = oci_core_vcn.main_vcn.default_route_table_id

  route_rules {
    network_entity_id = oci_core_internet_gateway.internet_gateway.id
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
  }
}

resource "oci_core_default_security_list" "default_security_list" {
  manage_default_resource_id = oci_core_vcn.main_vcn.default_security_list_id

  ingress_security_rules {
    icmp_options {
      code = 4
      type = 3
    }

    protocol = "1"
    source   = "0.0.0.0/0"
  }

  ingress_security_rules {
    icmp_options {
      type = 3
    }

    protocol = "1"
    source   = var.vcn_cidr_blocks[0]
  }

  egress_security_rules {
    protocol         = "all"
    destination_type = "CIDR_BLOCK"
    destination      = "0.0.0.0/0"
  }
}

resource "oci_core_nat_gateway" "nat_gateway" {
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.main_vcn.id

  display_name = "Nat Gateway ${var.vcn_display_name}"
}

resource "oci_core_route_table" "private_subnet_route_table" {
  compartment_id = var.compartment_id
  vcn_id         = oci_core_vcn.main_vcn.id

  display_name = "Route Table for Private Subnet"

  route_rules {
    network_entity_id = oci_core_nat_gateway.nat_gateway.id
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
  }

}

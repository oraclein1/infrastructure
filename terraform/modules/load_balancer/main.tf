resource "oci_core_network_security_group" "load_balancer" {
  compartment_id = var.compartment_id
  vcn_id         = var.vcn_id

  display_name = "Load Balancer Security Group"
}

resource "oci_core_network_security_group_security_rule" "load_balancer_ingress" {
  network_security_group_id = oci_core_network_security_group.load_balancer.id
  direction                 = "INGRESS"
  protocol                  = "6"
  source_type               = "CIDR_BLOCK"
  source                    = "0.0.0.0/0"
}

resource "oci_load_balancer_load_balancer" "load_balancer" {
  compartment_id = var.compartment_id
  display_name   = var.load_balancer_display_name
  shape          = var.load_balancer_shape
  subnet_ids     = var.load_balancer_subnets

  network_security_group_ids = [
    oci_core_network_security_group.load_balancer.id
  ]
}

resource "oci_load_balancer_backend_set" "app_server_backend_set" {
  health_checker {
    protocol    = "HTTP"
    port        = 8081
    return_code = 200
    url_path    = "/healthcheck"
  }

  load_balancer_id = oci_load_balancer_load_balancer.load_balancer.id
  name             = "app-server-backend-set"
  policy           = "ROUND_ROBIN"
}

resource "oci_load_balancer_load_balancer_routing_policy" "app_server_routing_policy" {
  condition_language_version = "V1"
  load_balancer_id           = oci_load_balancer_load_balancer.load_balancer.id
  name                       = "app_server_routing_policy"

  rules {
    actions {
      name             = "FORWARD_TO_BACKENDSET"
      backend_set_name = oci_load_balancer_backend_set.app_server_backend_set.name
    }
    condition = "all(http.request.url.path sw (i '/hello-world'))"
    name      = "hello_world_rule"
  }

  rules {
    actions {
      name             = "FORWARD_TO_BACKENDSET"
      backend_set_name = oci_load_balancer_backend_set.app_server_backend_set.name
    }
    condition = "all(http.request.url.path sw (i '/fibonacci'))"
    name      = "fibonacci_rule"
  }
}

resource "oci_load_balancer_listener" "app_server_backend_listener" {
  default_backend_set_name = oci_load_balancer_backend_set.app_server_backend_set.name
  load_balancer_id         = oci_load_balancer_load_balancer.load_balancer.id
  name                     = "app_server_backend_listener"
  port                     = 80
  protocol                 = "HTTP"
  routing_policy_name      = oci_load_balancer_load_balancer_routing_policy.app_server_routing_policy.name
}
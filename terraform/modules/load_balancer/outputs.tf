output "load_balancer_security_group_id" {
  value = oci_core_network_security_group.load_balancer.id
}

output "load_balancer_id" {
  value = oci_load_balancer_load_balancer.load_balancer.id
}

output "app_server_backend_set_name" {
  value = oci_load_balancer_backend_set.app_server_backend_set.name
}
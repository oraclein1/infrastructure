variable "compartment_id" {
  description = "OCI compartment in which the VCN will exist"
}

variable "vcn_id" {
  description = "VCN in which the load balancer will exist"
}

variable "load_balancer_display_name" {
  description = "Display name for load balancer"
}

variable "load_balancer_shape" {
  description = "Load balancer bandwidth shape"
}

variable "load_balancer_subnets" {
  description = "Subnets in which the load balancer will exist"
}
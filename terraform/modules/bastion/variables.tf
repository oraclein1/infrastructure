variable "availability_domain" {
  description = "Availability domain in which the Bastion instance is located"
}
variable "compartment_id" {
  description = "Compartment in which the Bastion instance will exist"
}

variable "vcn_id" {
  description = "VCN in which the load balancer will exist"
}

variable "tags" {
  description = "Tags to assign to the Bastion instance"
}

variable "instance_shape" {
  description = "Instance shape to launch the Bastion"
  default     = "VM.Standard.E2.1.Micro"
}

variable "ssh_public_key" {
  description = "SSH key for bastion"
}

variable "image_id" {
  description = "Machine image for instance"
}

variable "primary_subnet_id" {
  description = "Subnet for the Bastion instance"
}
resource "random_id" "bastion" {
  byte_length = 8
}

resource "oci_core_network_security_group" "bastion" {
  compartment_id = var.compartment_id
  vcn_id         = var.vcn_id

  display_name = "Bastion-Security-Group"
}

resource "oci_core_network_security_group_security_rule" "bastion_ssh" {
  network_security_group_id = oci_core_network_security_group.bastion.id
  direction                 = "INGRESS"
  protocol                  = "6"
  source_type               = "CIDR_BLOCK"
  source                    = "0.0.0.0/0"
  tcp_options {
    destination_port_range {
      max = 22
      min = 22
    }
  }
}

resource "oci_core_instance_configuration" "bastion" {
  compartment_id = var.compartment_id

  display_name  = "Bastion-${random_id.bastion.hex}"
  freeform_tags = var.tags

  instance_details {
    instance_type = "compute"

    launch_details {
      availability_domain = var.availability_domain
      compartment_id      = var.compartment_id
      shape               = var.instance_shape
      display_name        = "Bastion"

      create_vnic_details {
        assign_public_ip = true
        nsg_ids          = [oci_core_network_security_group.bastion.id]
      }

      metadata = {
        ssh_authorized_keys = var.ssh_public_key
      }

      source_details {
        source_type = "image"
        image_id    = var.image_id
      }
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "oci_core_instance_pool" "bastion" {
  compartment_id            = var.compartment_id
  instance_configuration_id = oci_core_instance_configuration.bastion.id
  placement_configurations {
    availability_domain = var.availability_domain
    primary_subnet_id   = var.primary_subnet_id
  }
  size = 1
}
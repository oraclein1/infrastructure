resource "random_id" "app_server" {
  byte_length = 8
}

resource "oci_core_network_security_group" "app_server" {
  compartment_id = var.compartment_id
  vcn_id         = var.vcn_id

  display_name = "App-Server-Security-Group"
}

resource "oci_core_network_security_group_security_rule" "app_server_ingress" {
  network_security_group_id = oci_core_network_security_group.app_server.id
  direction                 = "INGRESS"
  protocol                  = "6"
  source_type               = "NETWORK_SECURITY_GROUP"
  source                    = var.load_balancer_security_group_id
  tcp_options {
    destination_port_range {
      max = 8080
      min = 8080
    }
  }
}

resource "oci_core_network_security_group_security_rule" "app_server_ssh" {
  network_security_group_id = oci_core_network_security_group.app_server.id
  direction                 = "INGRESS"
  protocol                  = "6"
  source_type               = "NETWORK_SECURITY_GROUP"
  source                    = var.bastion_security_group_id
  tcp_options {
    destination_port_range {
      max = 22
      min = 22
    }
  }
}

resource "oci_core_network_security_group_security_rule" "app_server_healthcheck" {
  network_security_group_id = oci_core_network_security_group.app_server.id
  direction                 = "INGRESS"
  protocol                  = "6"
  source_type               = "NETWORK_SECURITY_GROUP"
  source                    = var.load_balancer_security_group_id
  tcp_options {
    destination_port_range {
      max = 8081
      min = 8081
    }
  }
}

resource "oci_core_instance_configuration" "app_server" {
  compartment_id = var.compartment_id

  display_name  = "App Server ${random_id.app_server.hex}"
  freeform_tags = var.tags

  instance_details {
    instance_type = "compute"

    launch_details {
      availability_domain = var.availability_domain
      compartment_id      = var.compartment_id
      shape               = var.instance_shape
      display_name        = "App Server"

      create_vnic_details {
        assign_public_ip = false
        nsg_ids          = [oci_core_network_security_group.app_server.id]
      }

      metadata = {
        ssh_authorized_keys = var.ssh_public_key
        user_data           = base64encode(file("${path.module}/userdata/cloud-config.yml"))
      }

      source_details {
        source_type = "image"
        image_id    = var.image_id
      }
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "oci_core_instance_pool" "app_server" {
  compartment_id            = var.compartment_id
  instance_configuration_id = oci_core_instance_configuration.app_server.id
  placement_configurations {
    availability_domain = var.availability_domain
    primary_subnet_id   = var.primary_subnet_id
  }
  size = 1

  load_balancers {
    backend_set_name = var.app_server_backend_set_name
    load_balancer_id = var.load_balancer_id
    port             = 8080
    vnic_selection   = "PrimaryVnic"
  }
}
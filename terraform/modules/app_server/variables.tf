variable "availability_domain" {
  description = "Availability domain in which the App Server instance is located"
}
variable "compartment_id" {
  description = "Compartment in which the App Server instance will exist"
}

variable "vcn_id" {
  description = "VCN in which the load balancer will exist"
}

variable "tags" {
  description = "Tags to assign to the App Server instance"
}

variable "instance_shape" {
  description = "Instance shape to launch the App Server"
  default     = "VM.Standard.E2.1.Micro"
}

variable "ssh_public_key" {
  description = "SSH key for App Server"
}

variable "image_id" {
  description = "Machine image for instance"
}

variable "primary_subnet_id" {
  description = "Subnet for the App Server instance"
}

variable "load_balancer_security_group_id" {
  description = "Allow ingress from load balancer"
}

variable "bastion_security_group_id" {
  description = "Allow SSH from bastion"
}

variable "load_balancer_id" {
  description = "ID of the load balancer containing the backend set"
}

variable "app_server_backend_set_name" {
  description = "Backend set to route to app server instance pool"
}
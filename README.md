# App Server Environment

How to provision the App Server environment
---
1. Make sure that your .oci config is configured for the DEFAULT profile
2. Navigate to the ./appserver
3. Modify the following locals to refelct your needs
   - compartment_id
   - vcn_cidr_bblocks
   - public_subnet_cidr_block
   - private_subnet_cidr_blcok
   - ssh_public_key
   - image_id
4. Run the following commands in order
   - `terraform init`
   - `terraform apply`

Improvements
---
- Create a folder structure that supports multiple environments
- Specify ssh public keys in cloud config
- Set up being able to run container on instsance restarts